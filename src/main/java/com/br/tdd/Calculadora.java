package com.br.tdd;

import java.math.BigDecimal;

public class Calculadora {

    public static int soma(int primeiroNumero, int segundoNumero){
        return primeiroNumero + segundoNumero;
    }

    public static double dividir(Integer primeiroNumero, Integer segundoNumero) {
        double resultado = primeiroNumero.doubleValue() / segundoNumero.doubleValue();
        return resultado;
    }

    public static double dividir(double primeiroNumero, double segundoNumero) {
        return primeiroNumero / segundoNumero;
    }

    public static int multiplicar(int primeiroNumero, int segundoNumero) {
        return primeiroNumero * segundoNumero;
    }

    public static double multiplicar(double primeiroNumero, double segundoNumero) {
        return primeiroNumero * segundoNumero;
    }
}
