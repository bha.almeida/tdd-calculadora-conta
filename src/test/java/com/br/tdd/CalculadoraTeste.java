package com.br.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CalculadoraTeste {

    @Test
    public void testarSomaDeDoisNumeros(){
        int resultado = Calculadora.soma(2, 2);
        Assertions.assertEquals(4, resultado);
    }

    @Test
    public void testarDivisaoNumerosInteiros(){
        double resultado = Calculadora.dividir(4, 2);
        Assertions.assertEquals(2, resultado);
    }

    @Test
    public void testarDivisaoNumerosInteirosResultandoFlutuante(){
        double resultado = Calculadora.dividir(10, 3);
        Assertions.assertEquals(3.3333333333333335, resultado);
    }

    @Test
    public void testarDivisaoNumerosFlutuantes(){
        double resultado = Calculadora.dividir(5.5, 2.5);
        Assertions.assertEquals(2.2, resultado);
    }

    @Test
    public void testarMultiNumerosInteiros(){
        double resultado = Calculadora.multiplicar(2, 5);
            Assertions.assertEquals(10, resultado);
    }

    @Test
    public void testarMultiNumerosFlutuantes(){
        double resultado = Calculadora.multiplicar(2.5, 2.5);
        Assertions.assertEquals(6.25, resultado);
    }

}
