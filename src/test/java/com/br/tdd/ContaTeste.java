package com.br.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ContaTeste {

    private Cliente cliente;
    private Conta conta;

    @BeforeEach
    public void setUp(){
        this.cliente = new Cliente("Bruno");
        this.conta = new Conta(this.cliente, 100.00);
    }

    @Test
    public void testarDepositoEmConta(){
        double valorDeDeposito = 400.00;
        conta.depositar(valorDeDeposito);

        Assertions.assertEquals(500.00, conta.getSaldo());
    }

    @Test
    public void testarSaqueEmConta(){
        double valorDeSaque = 50.00;
        conta.sacar(valorDeSaque);

        Assertions.assertEquals(50.00, conta.getSaldo());
    }

    //Teste exceção com LAMBDA
    @Test
    public void testarSaqueSemSaldoNaConta(){
        double valorDeSaque = 500.00;

        Assertions.assertThrows(RuntimeException.class, () -> {conta.sacar(valorDeSaque);});
    }
}
